﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessRules;
using Entities.Models;
using Entities.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController, Produces("application/json")]
    public class PostController : ControllerBase
    {
        private readonly PostBR postBR;
        public PostController(PostBR postBR)
        {
            this.postBR = postBR;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Post>), 200)]
        [ProducesResponseType(204)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(500)]
        public IActionResult Get()
        {
            try
            {
                var postsFind = this.postBR.GetAllPosts();
                if (postsFind.IsListObjectNull() || postsFind.IsEmptyListObject()) { return NoContent(); }

                return Ok(postsFind);
            }
            catch(Exception ex)
            {
                return StatusCode(500, new { Message = $"Internal server error {ex.Message}" });
            }
        }

        [HttpGet("{id}", Name = "PostById")]
        [ProducesResponseType(typeof(Post), 200)]
        [ProducesResponseType(204)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(500)]
        public IActionResult Get(Guid id)
        {
            try
            {
                var postFind = this.postBR.GetPostById(id);
                if (postFind.IsEmptyObject() || postFind.IsObjectNull()) { return NoContent(); }

                return Ok(postFind);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Message = $"Internal server error {ex.Message}" });
            }
        }

        [HttpPost]
        [ProducesResponseType(typeof(Post), 201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(500)]
        public IActionResult Post([FromBody]Post postNew)
        {
            if (postNew.IsObjectNull()) { return BadRequest(new { Message = "Post object is null" }); }
            if (!ModelState.IsValid) { return BadRequest(new { Message = "Invalid model object" }); }
            try
            {
                this.postBR.CreatePost(postNew);
                if (postNew.IsEmptyObject()) { return BadRequest(new { Message = "Post Object is not Created" }); }

                return CreatedAtRoute("PostById", new { id = postNew.Id }, postNew);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Message = $"Internal server error: {ex.Message}" });
            }
        }


        [HttpPut("{id}")]
        [ProducesResponseType(typeof(Post), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public IActionResult Put(Guid id, [FromBody]Post post)
        {
            try
            {
                if (id.Equals(Guid.Empty)) { return BadRequest(new { Message = "Id is Empty" }); }
                if (post.IsObjectNull()) { return BadRequest(new { Message = "Post Object is Null" }); }
                if (!ModelState.IsValid) { return BadRequest(new { Message = "Invalid model object" }); }

                bool secuence = this.postBR.UdpdatePost(id, post);

                if (!secuence) { return NotFound(); }

                return Ok(post);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Message = $"Internal server error: {ex.Message}" });
            }
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(405)]
        [ProducesResponseType(500)]
        public IActionResult Delete(Guid id)
        {
            try
            {
                if (id.Equals(Guid.Empty)) { return BadRequest(new { Message = "Id is Empty" }); }

                bool secuence = this.postBR.DeletePost(id);

                if (!secuence) { return StatusCode(405, new { Message = "Not allowed to delete Post registry." }); }

                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Message = $"Internal server error: {ex.Message}" });
            }
        }
    }
}