﻿using Contracts.Interfaces;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts.Models
{
    public interface ICommentRepository : IRepositoryBase<Comment>
    {
        IEnumerable<Comment> GetAllComments();

        Comment GetCommentById(Guid Id);

        void CreateComment(Comment comment);

        void UpdateComment(Comment dbComment, Comment comment);

        void DeleteComment(Comment comment);
    }
}
