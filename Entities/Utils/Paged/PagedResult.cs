﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities.Utils.Paged.Interfaces;

namespace Entities.Utils.Paged
{
    public class PagedResult<T> : PagedResultBase, IPagedResult<T> where T : class
    {
        public IEnumerable<T> Results { get; set; }

        public PagedResult()
        {
            Results = new List<T>();
        }
    }
}
