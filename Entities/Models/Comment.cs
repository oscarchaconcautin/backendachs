﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("Comment")]
    public class Comment : IEntity
    {
        [Key]
        public Guid Id { get; set; }

        [Column]
        public string Author { get; set; }

        [Column]
        public string Commentary { get; set; }

        [Column]
        public DateTime CreateDate { get; set; }

        public Post Post { get; set; }
    }
}
