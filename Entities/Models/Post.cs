﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Entities.Models
{
    [Table("Post")]
    public class Post: IEntity
    {
        [Key]
        public Guid Id { get; set; }

        [Column]
        public DateTime CreateDate { get; set; }

        [Column]
        public string Title { get; set; }

        [Column]
        public string Description { get; set; }

        [Column]
        public string ImageURL { get; set; }

        [Column]
        public string Author { get; set; }

        public IList<Comment> Comments { get; set; }

        public IList<PostTag> PostTags { get; set; }

    }
}
