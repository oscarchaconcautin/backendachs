﻿using Contracts.Models;
using Entities;
using Entities.Models;
using Entities.Models.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository.Models
{
    public class CommentRepository : RepositoryBase<Comment>, ICommentRepository
    {
        public CommentRepository(RepositoryContext repository) : base(repository) { }

        public void CreateComment(Comment comment)
        {
            comment.Id = new Guid();
            comment.CreateDate = DateTime.UtcNow;
            this.Create(comment);
        }

        public void DeleteComment(Comment comment)
        {
            this.Delete(comment);
        }

        public IEnumerable<Comment> GetAllComments()
        {
            var commentsFind = this.FindAll();

            commentsFind = commentsFind.OrderBy(post => post.CreateDate);

            return commentsFind.ToList();
        }

        public Comment GetCommentById(Guid Id)
        {
            var commentFind = this.FindByCondition(comment => comment.Id.Equals(Id))
                                    .DefaultIfEmpty(new Comment())
                                    .FirstOrDefault();
            return commentFind;
        }

        public void UpdateComment(Comment dbComment, Comment comment)
        {
            dbComment.Map(comment);
            this.Update(dbComment);
        }
    }
}
