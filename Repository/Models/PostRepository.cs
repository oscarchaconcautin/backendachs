﻿using Contracts.Models;
using Entities;
using Entities.Models;
using Entities.Models.Extensions;
using Entities.Utils.Paged;
using Entities.Utils.Paged.Interfaces;
using Repository.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository.Models
{
    public class PostRepository : RepositoryBase<Post>, IPostRepository
    {
        public PostRepository(RepositoryContext repository) : base(repository) { }

        public void CreatePost(Post post)
        {
            post.Id = new Guid();
            post.CreateDate = DateTime.UtcNow;
            this.Create(post);
        }

        public void DeletePost(Post post)
        {
            this.Delete(post);
        }

        public IPagedResult<Post> GetAllPostPaged(int? page = null, int? pageSize = null, Guid? tagId = null)
        {
            var postsFind = this.FindAll();
            if (tagId != null)
            {
                var postTagsFind = this.RepositoryContext
                                        .PostTags
                                        .Where(postTag => postTag.TagId.Equals(tagId.Value))
                                        .Select(postTag => postTag.PostId);
                postsFind = postsFind.Where(post => postTagsFind.Contains(post.Id));
            }

            if (page != null && pageSize != null)
            {
                return postsFind.GetPaged(page.Value, pageSize.Value);
            }

            return new PagedResult<Post>
            {
                Results = postsFind.ToList()
            };



        }

        public IEnumerable<Post> GetAllPosts()
        {
            var postsFind = this.FindAll();

            postsFind = postsFind.OrderBy(post => post.CreateDate);

            return postsFind.ToList();
        }

        public Post GetPostById(Guid Id)
        {
            var postFind = this.FindByCondition(post => post.Id.Equals(Id))
                                    .DefaultIfEmpty(new Post())
                                    .FirstOrDefault();
            return postFind;
        }

        public void UpdatePost(Post dbPost, Post post)
        {
            dbPost.Map(post);
            this.Update(dbPost);
        }
    }
}
