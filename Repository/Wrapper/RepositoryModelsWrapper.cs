﻿using Contracts.Models;
using Entities;
using Repository.Interfaces;
using Repository.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.Wrapper
{
    public class RepositoryModelsWrapper : IRepositoryModelsWrapper
    {
        private readonly RepositoryContext repository;

        IPostRepository post;

        ITagRepository tag;

        ICommentRepository comment;

        public RepositoryModelsWrapper(RepositoryContext repository)
        {
            this.repository = repository;
        }

        public IPostRepository Post
        {
            get
            {
                if (this.post == null)
                {
                    this.post = new PostRepository(this.repository);
                }
                return this.post;
            }
        }

        public ITagRepository Tag
        {
            get
            {
                if (this.tag == null)
                {
                    this.tag = new TagRepository(this.repository);
                }

                return this.tag;
            }
        }

        public ICommentRepository Comment
        {
            get
            {
                if (this.comment == null)
                {
                    this.comment = new CommentRepository(this.repository);
                }
                return this.comment;
            }
        }

        public void Save()
        {
            this.repository.SaveChanges();
        }
    }
}
