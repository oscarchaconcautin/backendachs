﻿using System;
using System.Collections.Generic;
using System.Text;
using Contracts.Models;

namespace Repository.Interfaces
{
    public interface IRepositoryModelsWrapper
    {
        IPostRepository Post { get; }

        ITagRepository Tag { get; }

        ICommentRepository Comment { get; }

        void Save();
    }
}
