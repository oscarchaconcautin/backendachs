﻿using Entities.Models;
using Entities.Utils;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessRules
{
    public class PostBR
    {
        private readonly IRepositoryModelsWrapper repository;
        public PostBR(IRepositoryModelsWrapper repository)
        {
            this.repository = repository;
        }


        public IEnumerable<Post> GetAllPosts()
        {
            try
            {
                var postsFind = this.repository.Post.GetAllPosts();
                return postsFind;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        public Post GetPostById (Guid Id)
        {
            try
            {
                var postFind = this.repository.Post.GetPostById(Id);
                return postFind;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        public void CreatePost(Post postNew)
        {
            try
            {
                this.repository.Post.CreatePost(postNew);
                this.repository.Save();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        public bool UdpdatePost(Guid postId, Post postUpdated)
        {
            try
            {
                var dbPost = this.repository.Post.GetPostById(postId);
                if (dbPost.IsEmptyObject() || dbPost.IsObjectNull()) { return false; }

                this.repository.Post.UpdatePost(dbPost, postUpdated);
                this.repository.Save();

                postUpdated.Id = postId;

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        public bool DeletePost(Guid postId)
        {
            try
            {
                var dbPost = this.repository.Post.GetPostById(postId);
                if (dbPost.IsEmptyObject()) { return false; }


                this.repository.Post.DeletePost(dbPost);
                this.repository.Save();

                return true;
            }
            catch (ArgumentNullException anex)
            {
                throw new Exception(anex.Message, anex.InnerException);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }
    }

    
}
